//This is a global variable since it was made outside a function all can see and fuck with it.
var OG_Rolls=[];
var stats = ['Str', 'Dex', 'Con', 'Int', 'Wis', 'Cha'];
//This is both the onClick and onChange event Listeners
{

document.addEventListener('click', function(event){
	if(event.target.className === 'traits'){
		traits();
	}
	if(event.target.id === "reroll"){
		if(document.getElementById('dice').value === 'legendary'){
			legendary();
		}else if(document.getElementById('dice').value === 'normal'){
			standard();
		}else if(document.getElementById('dice').value === 'weak'){
			peasent();
		}
		mods();
		pfTable();
	}
	skillPointHandler();
	skills();
	if (event.target.classList.contains('statButton')){
		statsHandler(event.target.id)
		mods();
	}
	raceHandler();
	tableHighlighter();

});

document.addEventListener('change', function(event){
	
	if(event.target.value === "5e" || event.target.value === 'pf'){
		editionHandler(event);
	}
	if(event.target.value === 'buy'){
			pointBuy();
	}

	
	skillPointHandler();
	raceHandler();
	mods();
	skills();
	skillPoints();
	feats();
});
}

//These are just return values to get Stat Mod Scores easier
{
	function strMod(){
		return document.getElementById('StrMod').value;
	}
	function dexMod(){
		return document.getElementById('DexMod').value;
	}
	function conMod(){
		return document.getElementById('ConMod').value;
	}
	function intMod(){
		return document.getElementById('IntMod').value;
;
	}
	function wisMod(){
		return document.getElementById('WisMod').value;
	}
	function chaMod(){
		return document.getElementById('ChaMod').value;

	}
}

//This by name alone gets the traits.
	// I want to eventually move it
//TODO update this.
	//update so traits can't duplicate.
function traits(){
	
	var traitList = ["Adventurous","Affable","Capable","Cruel","Charming","Confident","Conscientious","Cultured","Dependable","Disloyal","Discreet","Easily Stressed","Encouraging","Exuberant","Fearless","Greedy","Gregarious","Helpful",	"Humble","Mean","Meticulous","Merciful","Narcissistic","Obnoxious","Observant","Optimistic","Pettie","Persistent","Pessimistic","Reliable","Sneaky","Sociable","Trusting","Unforgiving","Valiant","Vulgar"];
	
	if(event.target.type === 'button'){
		if(event.target.innerHTML === 'Traits'){
			for(var x = 1; x < 6 ; x++){
				document.getElementsByClassName('traits')[x].innerHTML = traitList[Math.floor(Math.random() * traitList.length)];
			}
		}else{
			event.target.innerHTML = traitList[Math.floor(Math.random() * traitList.length)];
		}
	}
}
//This will set the Max feats a character should have based on level
function feats(){
	if(document.getElementById('edition').value === "pf"){
		document.getElementById('totalFeats').value = Math.floor(document.getElementById('level').value/2+.5);
	}else if(document.getElementById('edition').value === "5e"){
		document.getElementById('totalFeats').value = Math.floor(document.getElementById('level').value/4);
	}
}
//This Will randomly select the NPCs name
function nameSet(){
	
}
//this will change the bonus stats based on level as well, although this is just bad for 5e
	//todo fix feats and skillpoints cause 5e is shit
		//5e is fucking stupid
function skillPoints(){
	var level = document.getElementById('level');
	var skills = document.getElementById('bonusStats');
	skills.value = Math.floor(level.value/4);
}
/*
IMPORTANT: The dice rolls will roll down, class / race will not change the rolls
Race will eventually be added to add the racial bonuses

*/
//Dice Roll scripts
function legendary(){
	
	//this is to be the dice, will be randomized 1-6
	var dice;
	//this will act as the total for each stat roll
	var total=0;
	//this should hold the OG_Rolls max of 5
	var jail=[];
	//This array will be ran 5 times 
	for(var y = 0; y < 6; y++){
		//the inner loop is the dice rolling part
		for(var x = 0; x < 5; x++){
			//this is the magic script, this will "roll" five 6-sided dice
			dice =	Math.floor((Math.random() * 6)+1);
			//if the dice is a 1 it'll reroll the number
			if(dice !== 1){
				//if the dice is 1, adds it to dice jail
					//this is so the last 2 digits can be removed
				jail[x] = dice;
				//adds the dice to the total for later use
				total = total + dice;
			}else{
				x--;
			}
		}
		//this will sort the 5 rolled dice, into stat rolls
		jail.sort((a,b)=>b - a);
		//the total is reduced by the lowest 2 rolls in the 5 dice line up to insure fire stat rolls everytime
		total -= jail[4];
		total -=jail[3];
		//rolls are placed in the stat order they were rolled
		document.getElementsByClassName('Stat')[y].value = total;
		OG_Rolls[y] = total;
		total = 0;
	//end of the loop for the dice rolling
	}
}
function standard(){
	
	var dice;
	var total=0;
	var jail=[];
	for(var y = 0; y < 6; y++){
		for(var x = 0; x < 4; x++){
			dice =	Math.floor((Math.random() * 6)+1);
				jail[x] = dice;
				total = total + dice;

		}
		jail.sort((a,b)=>b - a);
		total -=jail[3];
		OG_Rolls[y] = total;
		document.getElementsByClassName('Stat')[y].value = total;
		total = 0;
	}
}
function peasent(){
	
	var dice;
	var total=0;
	var jail=[];
	for(var y = 0; y < 6; y++){
		for(var x = 0; x < 3; x++){
			dice =	Math.floor((Math.random() * 6)+1);
				jail[x] = dice;
				total = total + dice;
		}
		jail.sort((a,b)=>b - a);
		document.getElementsByClassName('Stat')[y].value = total;
		OG_Rolls[y]=total;
		total = 0;
	}
}

//this sets the modifiers after the dice are rolled
function mods(){
	
	var x = 0;
	//stats is a global value ( stats = ['Str', 'Dex', 'Con', 'Int', 'Wis', 'Cha']; )
		//so it will cycle through all stats
		stats.forEach(toMod =>{
			//gets the stat value
			var newRoll = document.getElementById(toMod).value;
			//sets the modifier in the table 
			//sets the statMod [StrMod] 
				//modifier math is score-10 / 2 rounded down cause that's how the rules work
			document.getElementById(toMod+'Mod').value = Math.floor((newRoll-10)/2);
			x++;
		});	

		x = 0;
	
	//This will do the same as above but in the OG table instead
		stats.forEach(toMod =>{
			document.getElementById('OG'+toMod+'Mod').value = Math.floor((OG_Rolls[x]-10)/2);
			x++;
		});	
}
  
//This will be to auto-add racial bonuses
	//i hate myself for doing this tbh
function raceHandler(){
	// variable setting
	//we need to know what edition we're on and what race we are
		//todo race isn't set yes and needs to be set sooner or later
	var version = document.getElementById('edition');
	var race = document.getElementById('race').value;
	//checks edition and to make sure the stats were rolled
		//OG_Rolls.legnth took me a bit longer than it should have
			//cringe i want you to cringe
	if(OG_Rolls.length !== 0 && version.value === 'pf'){
//	This is a wrong version as the line above: if(OG_Rolls != false){
		resetStats();
		pfRaceBonus(race);
	}
	if(OG_Rolls.length !== 0 && version.value === '5e'){
		resetStats();
		fithEiditionRaceBonus(race);
	}
}	 

//This will reset the stats back to their OG values
function resetStats(){
	var x = 0;
	//this is for the main stats that will change when race is selected
	stats.forEach(theStats =>{
		document.getElementById(theStats).value = OG_Rolls[x];
		x++;
	});
	x = 0;
	//This will make sure the Original stats are shown
		//todo make this it's own function so it only has to fire once
			//it works now so not to important
	stats.forEach(theStats =>{
		document.getElementById('OG'+theStats).value = OG_Rolls[x];
		x++;
	});

}
//RaceBonus() scripts are only to change the color of stat Scores based on if they recived a positive or negitive buff
	//this is why i hate the race handler it just points to this, i wish i'd do it like java and make new classes for functions
		//how does importing work?
function pfRaceBonus(raceName){
//	alert(OG_Rolls);
	//this table is for all the bonuses
		//i wonder if i can make this into a JSON file
			//will try that one day later
	var statMods = {
		none: {},
		elf: {Int:2,Wis:2,Con:-2
		},
		weeman:{Con:2,Wis:2,Cha:-2
		},
		orc:{Str:4,Int:-2,Wis:-2
		},
		kobold:{Dex:2,Str:-4,Con:-2
		},
		goblin:{Dex:4,Str:-2,Cha:-2 
		},
		kitsune:{Dex:2,Cha:2,Str:-2
		}
	};
	//streams all of the stats to check against the statMods List
	stats.forEach(theStats =>{
		if(statMods[raceName][theStats] != null){
			
//streams the race to get the value
//If the Values don't align with the original rolled values it changes the color to green if positive, red if negitive.
			document.getElementById(theStats).value = parseInt(document.getElementById(theStats).value) + statMods[raceName][theStats];
			
			if(statMods[raceName][theStats] > 0){
				document.getElementById(theStats).style.backgroundColor = 'green';
			}
			else{
				document.getElementById(theStats).style.backgroundColor = 'red';
			}
		}
		else{
			document.getElementById(theStats).style.backgroundColor = '#ffffff';
		}
	});
}
function fithEiditionRaceBonus(raceName){
//	alert(OG_Rolls);
	var stats = ['Str','Dex','Con','Int','Wis','Cha'];
	var statMods = {
		none: {},
		elf: {
			Dex:2, 
			Con:1
			 },
		weeman:{
			Con:2
			   },
		orc:{Str:2,Con:1
			},
		kobold:{
			Dex:2,
			Int:1
			   },
		goblin:{
			Dex:2,
			Con:1
			   },
		tabaxi:{
			Dex:2, 
			Cha:1
		},
		warforged:{
			Con:2
		}
	};
	stats.forEach(theStats =>{
		if(statMods[raceName][theStats] != null){
//streams the race to get the value
//If the Values don't align with the original rolled values it changes the color to green if positive, red if negitive.
			document.getElementById(theStats).value = parseInt(document.getElementById(theStats).value) + statMods[raceName][theStats];
			document.getElementById(theStats).style.backgroundColor = 'green';
		}
		else{
			document.getElementById(theStats).style.backgroundColor = '#ffffff';
		}
		
	});
}
//This is to hide and unhide edition specific items
	//right now it only does races cause that's the only thing i have set
		//Original this would unhide a 5e table but i have a script to build tables
function editionHandler(){
	var race = document.getElementById("race");
	var raceEdition = race.options[race.selectedIndex].className;	
	var x;
	var version = document.getElementById('edition').value;
	var edition5 = $('.5e');
	var editionPf = $('.pf');
	
//	this doesn't work fix it 
	if(raceEdition !== document.getElementById('edition').className){
		document.getElementById('race').value = "none";
	}
	
	if(version === '5e'){
		for(x = 0; x < edition5.length; x++){
		edition5[x].hidden = false;
		}
		for(x = 0; x < editionPf.length; x++){
			editionPf[x].hidden = true;
		}
	}
	
	if(version === 'pf'){
		for(x = 0; x < editionPf.length; x++){
			editionPf[x].hidden = false;
		}
		for(x = 0; x < edition5.length; x++){
			edition5[x].hidden = true;
		}
	}	
}

// This is a prime example of why you comment when you make a new script idk wtf this does
	
//This sets the Original stats or is linked to it some how

function skills(){
	var skillsL = document.getElementsByClassName('skillMod');
	for(var x = 0; x < skillsL.length; x++){
		var actual = document.getElementById(skillsL[x].classList.item(1)+"Mod").value;
		switch(true){
			case skillsL[x].classList.contains('Dex'):
				skillsL[x].value = actual;
				break;
			case skillsL[x].classList.contains('Int'):
				skillsL[x].value = actual;
				break;
			case skillsL[x].classList.contains('Str'):
				skillsL[x].value = actual;
				break;
			case skillsL[x].classList.contains('Cha'):
				skillsL[x].value = actual;
				break;
			case skillsL[x].classList.contains('Wis'):
				skillsL[x].value = actual;
				break;
			default:
				break;
		}
	}
}

//This is for point-buy or when you need to add skill points
//just handels all the events and calls everything to check
function skillPointHandler(){
	//these vars are set to be passed on, basically to check if something has a value of "buy"
	var dice = document.getElementById('dice');
	var race = document.getElementById('race');
	var buttons = document.getElementsByClassName('statButton');
	theButtons(dice, race, buttons);
}

function theButtons(dice, race, buttons){
	var bonus = document.getElementById('bonusStats');
	if(bonus.value >= 0){
		for(var x = 0; x < buttons.length; x++){
			buttons[x].hidden = false;
		}
	}
	if(bonus.value < 1){
		for(var x = 0; x < buttons.length; x++){
			buttons[x].hidden = true;
		}
	}
}

function statsHandler(node){
	if(event.target.classList.contains('plus')){
			addingButton(node);
		}
		if(event.target.classList.contains('minus')){
			subButton(node);
		}
}

//TODO work on this and make Sub button
function addButton(){
	
}
//function addingButton(node){

//This comment hides ALL the OG point-buy
//	{
////	var bonus = document.getElementById('bonusStats');
////	//this gets the type of modifier
////		//plusStrButton is cut up to just get Str
////	var target = node.substring(4, 7);
////	//This gets the stat value from the target
////	var targetMod = document.getElementById(target);
////	//this adds a +1 to that value when it's pressed
////	targetMod.value = parseInt(targetMod.value)+1;
////	//this is the button that was clicked
////	var clicked = document.getElementById(node);
////	//this is the oppisite to the button that was clicked
////	var notClicked = document.getElementById(node.replace('plus', 'minus'));
////	var nextPay = 0;
//////This is going to do the point spending
////	switch(parseInt(targetMod.value)){
////		case 8:
////			notClicked.hidden = false;
////			bonus.value = Number(bonus.value)-2;
////		case 9:
////			bonus.value = Number(bonus.value)-1;
////			nextPay = 1;break;
////		case 10:
////			bonus.value = Number(bonus.value)-1;nextPay = 1;break;
////		case 11:
////			bonus.value -= 1;nextPay = 1;break;
////		case 12:
////			bonus.value -= 1;nextPay = 1;break;
////		case 13:
////			bonus.value -= 1;nextPay = 2;break;
////		case 14:
////			bonus.value -= 2;nextPay = 2;break;
////		case 15:
////			bonus.value -= 2;nextPay = 3;break;
////		case 16:
////			bonus.value -= 3;nextPay = 3;break;
////		case 17:
////			bonus.value -=3;nextPay = 4;break;
////		case 18:
////			bonus.value -= 4;
////			clicked.hidden = true;break;
////		default:
////			break;
////	}
//	}
//}
//function subButton(node){
//	{//This comments out ALL the OG point-buy 
////	var bonus = document.getElementById('bonusStats');
////	var target = node.substring(5, 8);
////	var targetMod = document.getElementById(target);
////	targetMod.value = parseInt(targetMod.value)-1;
////	var clicked = document.getElementById(node);
////	var notClicked = document.getElementById(node.replace('minus', 'plus'));
////
////	switch(parseInt(targetMod.value)){
////		case 7:
////			bonus.value = Number(bonus.value)+2;
////			clicked.hidden = true ;break;
////		case 8:
////			bonus.value = Number(bonus.value)+1;break;
////		case 9:
////			bonus.value = Number(bonus.value)+1;break;
////		case 10:
////			bonus.value = Number(bonus.value)+1; break;
////		case 11:
////			bonus.value = Number(bonus.value)+1;break;
////		case 12:
////			bonus.value = Number(bonus.value)+1;break;
////		case 13:
////			bonus.value = Number(bonus.value)+2;break;
////		case 14:
////			bonus.value = Number(bonus.value)+2;break;
////		case 15:
////			bonus.value = Number(bonus.value)+3;break;
////		case 16:
////			bonus.value = Number(bonus.value)+3;break;
////		case 17:
////			notClicked.hidden = false;
////			bonus.value = Number(bonus.value)+4;break;
////		default:
////			break;
////	
////	}
//}
//}



function tableHandler(){
	var theTable = document.getElementById('table');
	

}
function clearTable(theTable){
	for(var reset = 0; reset < theTable.childNodes.length; reset++){
		theTable.removeChild(theTable.childNodes[reset]);
	}
}
function pfTable(){
	
	var dataBase = {
		//I was REALLY lazy so this is the header
			//is there a different between a normal cell and header cells?
				//there in fact is and i just don't care
		//will be iterated through to lazy to make it nice cause this works.
		'Skill Name':{type:'Stat Type',skillMod:'Stat Mod'},
		Acrobatics: {type: 'Dex',skillMod: dexMod()},
	  	Appraise: {type: 'Int',skillMod: intMod()},
		Bluff:{type:'Cha',skillMod:chaMod()},
		Climb:{type:'Str',skillMod:strMod()},
		Craft:{type:'Int',skillMod:intMod()},
		Diplomacy:{type:'Cha',skillMod:chaMod()},
		'Escape Artist':{type:'Dex',skillMod:dexMod()},
		Fly:{type:'Dex',skillMod:dexMod()},
		'Handle Animal':{type:'Cha',skillMod:chaMod()},
		Heal:{type:'Wis',skillMod:wisMod()},
		Intimidate:{type:'Cha',skillMod:chaMod()},
		'Knowledge(Arcane)':{type:'Int',skillMod:intMod()},
		'Knowledge(Dungeoneering)':{type:'Int',skillMod:intMod()},
		'Knowledge(Engineering)':{type:'Int',skillMod:intMod()},
		'Knowledge(Geography)':{type:'Int',skillMod:intMod()},
		'Knowledge(History)':{type:'Int',skillMod:intMod()},
		'Knowledge(Local)':{type:'Int',skillMod:intMod()},
		'Knowledge(Nature)':{type:'Int',skillMod:intMod()},
		'Knowledge(Nobility)':{type:'Int',skillMod:intMod()},
		'Knowledge(Planes)':{type:'Int',skillMod:intMod()},
		'Knowledge(Religion)':{type:'Int',skillMod:intMod()},
		'Linguistics':{type:'Int',skillMod:intMod()},
		'Perception':{type:'Wis',skillMod:wisMod()},
		'Perform':{type:'Cha',skillMod:chaMod()},
		'Profession':{type:'Wis',skillMod:wisMod()},
		'Ride':{type:'Dex',skillMod:dexMod()},
		'Sense Motives':{type:'Wis',skillMod:wisMod()},
		'Sleight of Hands':{type:'Dex',skillMod:dexMod()},
		'Spellcraft':{type:'Int',skillMod:intMod()},
		'Stealth':{type:'Dex',skillMod:dexMod()},
		'Survival':{type:'Wis',skillMod:wisMod()},
		'Swim':{type:'Str',skillMod:strMod()},
		'Use Magic Device':{type:'Cha',skillMod:chaMod()},
};
	
	//This selects the table (shocker)
	var theTable = document.getElementById('table');
	theTable.hidden = false;
	//This will delete the table before it rebuilds it 
		//this is so everytime something changes there's no duplacate table
	clearTable(theTable);
	
	//This is the fake of the script, does all the work
	
	//Will loop though all the information in database
		//var skillName is the current key of dataBase (ie Acrobatics, Appraise, Buff and so on)
	for(var skillName in dataBase){
		
		//This puts everything into a textNode so it can be added to the table
		var skill = document.createTextNode(skillName);
		//Will make a new row each time it's called
			//also used to take cells in the next half
		var newRow = theTable.insertRow();
		
		//This makes it so you can add cells, called a bit early so i can use it for Skill Names
			//Note this is done here so it runs properly, need a new Cell for the title of the Skill (acro, appraise, bluff...) each time AFTER the addintional info is added
		var newSkill = newRow.insertCell();
		newSkill.appendChild(skill);
		newSkill.classList.add('Table');
		//This will loop through the information in dataBase the *guts* of Acrobatics, Appraise, Buff...
			//The information this reads is the VALUES of type and skillMod (ie if it's dex, str, con, int, cha, wis) and the skill modifier from the main table
	for(var informationName in dataBase[skillName]){
		//a repeat of newSkill tbh i could probably replace it but it was good to me while making this so i keep
		var newStat = newRow.insertCell();
		//puts the guts into a textNode to be placed in the cell
		var innerd = document.createTextNode(dataBase[skillName][informationName]);
		//puts it all into a cell
		newStat.appendChild(innerd);
		newStat.classList.add('Table', 'tableSkillMods');
		//This will look for specifically the Mod numbers and put them into a class of their own
			//their class has .CSS attached and will be added on too.
		if(informationName === "type" && skillName != 'Skill Name'){
			//This gets the "type" of the skill to add to the stat mod as a class so all dex stats are notied as such.
			var skillsClass = dataBase[skillName][informationName];
		}
		if(informationName === "skillMod" && skillName != 'Skill Name'){
			//this add a classList to the skill mods.
			//pfModTable will be for later to go through all the skills and update their numbers on change.
			newStat.classList.add('pfModTable', skillsClass);
		}
	}
	}
}
function fifthTable(){
	
}
function tableHighlighter(){
	  $('#table tr').hover(function() {
          $(this).toggleClass('hover');
    });
}
