~~This is being built~~ to make character creation easier. Currently allows you to make a character using either 5e or pathfinder 1e rulesets.
Although it only allows for limited creation.
This webpage will generate stats randomly to each skill and print a table of skills depending on the edition.

### Roadmap possibilities

- Add more Races apon request with respected bonuses
- Point-buy system
- Add a feats list to choose from randomly based on max feats
- Add a backstory Generator based on small chunks
- Make the Traits into a table so each trait can be explained under it
	- Traits such as one handed.
- Table of Actions
	- such as how the character may act in situations.
- Make a table for class features and what they do
	- such as rogue sneak attack or trap sense.
- Maybe make a table explaining how a class may function
	- To explain classes with complex abilities, to help new players.